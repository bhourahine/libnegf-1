/*
 * Copyright 1997, Regents of the University of Minnesota
 *
 * metis.h
 *
 * This file includes all necessary header files
 *
 * Started 8/27/94
 * George
 *
 * $Id: metis.h 2501 2007-11-20 02:33:29Z benkirk $
 */

/*
#define	DEBUG		1
#define	DMALLOC		1
*/

#include "./stdheaders.h"

#ifdef DMALLOC
#include <dmalloc.h>
#endif

#include "./defs.h"
#include "./struct.h"
#include "./macros.h"
#include "./rename.h"
#include "./proto.h"

